#include <iostream>         //potrzebne do uzywania strumienia wejscia i wyjscia
#include <string>           //potrzebne do uzywania zmiennych jako ciagu znakow

using namespace std;        //po prostu musi byc ;P

int main()      //glowna funkcja, musi byc w kazdym programie
{
    string Imie;                //utworzenie zmiennej typu string czyli ciag znakow
    string Rok_urodzenia;
    string Ulubiony_kolor;

    cout<<"Podaj imie"<<endl;               //wyswietla na ekranie tekst w cudzyslowiu
    cin>>Imie;                              //przypisuje zmiennej Imie wpisana wartosc
    cout<<"Podaj rok urodzenia"<<endl;
    cin>>Rok_urodzenia;
    cout<<"Podaj ulubiony kolor"<<endl;
    cin>>Ulubiony_kolor;
    
    cout<<"\nimie: "<<Imie<<"\nrok urodzenia: "<<Rok_urodzenia<<"\nulubiony kolor: "<<Ulubiony_kolor<<endl;     //wyswietla tekst w cudzyslowiu i wartosci zmiennych, \n dziala jak enter, przenosi do nastepnej linijki

    return 0;   //zwraca wartosc funkcji jako 0, wytlumacze bardziej jak bedziesz miec funkcje
}
