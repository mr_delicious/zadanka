#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    srand(time(0));         //sprawia ze za kazdym razem wartosc losowej liczby jest losowana ponownie, bez tego przy kazdym uzyciu programu losowa loczba bylaby taka sama
    cout<<"Program losuje liczbe od 1 do 5. Zgadnij ja"<<endl;
    
    int odpowiedz=0;
    cin>>odpowiedz;
    
    int losowa_liczba=(rand()%5)+1;     //utworzenie zmiennej i przypisanie losowej liczby miedzy 1 a 5
    
    if (odpowiedz==losowa_liczba)
        cout<<"Gratulacje! Zgadles liczbe!"<<endl;
    else
        cout<<"bardzo mi przykro ale wylosowana liczba to: "<<losowa_liczba<<endl;
    return 0;
}