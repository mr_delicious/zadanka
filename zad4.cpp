#include <iostream>

using namespace std;

int main()
{
    cout<<"Podaj trzy liczby calkowite"<<endl;
    int a=0;
    int b=0;
    int c=0;
    cin>>a>>b>>c;
    
    if (b>a && b>c)     //jesli b>a i b>c czyli sprawdza czy b jest najwieksze
        {
            if (b*b==a*a+c*c)       //sprawdza czy kwadrat najwiekszej liczby jest rowny sumie kwadratow mniejszych liczb
                cout<<"Liczby stanowia trojke pitagorejska"<<endl;
            else
                cout<<"To nie jest trojka pitagorejska"<<endl;
        }
    else if (c>a && c>b)    //jesli b nie jest najwieksze to sprawdza czy c jest najwieksze
        {
             if (c*c==a*a+b*b)
                cout<<"Liczby stanowia trojke pitagorejska"<<endl;
            else
                cout<<"To nie jest trojka pitagorejska"<<endl;
        }
    else        //jesli ani b ani c nie jest najwieksze
        {
          if (a*a==c*c+b*b)
               cout<<"Liczby stanowia trojke pitagorejska"<<endl;
           else
               cout<<"To nie jest trojka pitagorejska"<<endl;
        }
    
    

    return 0;
}