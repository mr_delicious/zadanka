#include <iostream>

using namespace std;

int main()
{
    int rok=0;                                                  //utworzenie zmiennej int czyli calkowitej
    cout<<"Podaj rok"<<endl;
    cin>>rok;
    if ((rok%4==0 && rok%100!=0) || (rok%400==0))               //instrukcja warunkowa if, jesli rok (jest podzielny (%) przez 4 i (&&) przez 100) lub (||) rok jest podzielny przez 400
        cout<<"Rok "<<rok<<" jest przestepny"<<endl;            //kiedy przy if lub else jest tylko jedna instrukcja nie trzeba dawac nawiasu, przy wiekszej liczbie instrukcji trzeba dac nawias klamrowy
    else                                                        //instrukcja spelniajaca sie jesli warunek przy if nie zostanie spelniony
        cout<<"Rok "<<rok<<" nie jest przestepny"<<endl;
    return 0;